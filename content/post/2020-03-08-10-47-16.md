---
title: "Where did the love go? In today’s black pop music, love songs are harder to find"
date: "2020-03-08T10:47:16"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://theundefeated.com/features/where-did-the-love-go-in-todays-black-pop-music-love-songs-are-harder-to-find/) and an archived link [here](https://web.archive.org/web/20200308154829/https://theundefeated.com/features/where-did-the-love-go-in-todays-black-pop-music-love-songs-are-harder-to-find/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._