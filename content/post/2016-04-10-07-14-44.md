---
title: "Money for Nothing: The Lucrative World of Club Appearances"
date: "2016-04-10T07:14:44"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.gq.com/story/how-celebs-get-paid-for-club-appearances) and an archived link [here](https://web.archive.org/web/20160405223015/http://www.gq.com/story/how-celebs-get-paid-for-club-appearances).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









