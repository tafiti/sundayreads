---
title: "From Africa To Kent: Following In The Footsteps Of Migrants"
date: "2014-12-14T07:54:35"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newstatesman.com/world-affairs/2014/12/africa-kent-following-footsteps-migrants?ns_mchannel=email&ns_source=inxmail_newsletter&ns_campaign=bbcnewsmagazine_news__&ns_linkname=na&ns_fee=0) and an archived link [here](https://web.archive.org/web/20141211110758/http://www.newstatesman.com/world-affairs/2014/12/africa-kent-following-footsteps-migrants).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









