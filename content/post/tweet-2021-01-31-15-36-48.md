---
title: "Marcus Rashford: the making of a food superhero | Marcus Rashford | The Guardian"
date: "2021-01-31T15:36:48"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/football/2021/jan/17/marcus-rashford-the-making-of-a-food-superhero-child-hunger-free-school-meals) and an archived link [here](http://web.archive.org/web/20210117080848/https://www.theguardian.com/football/2021/jan/17/marcus-rashford-the-making-of-a-food-superhero-child-hunger-free-school-meals).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
