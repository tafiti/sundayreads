---
title: "The Lunar Sea"
date: "2017-08-13T08:46:42"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.hakaimagazine.com/features/lunar-sea/) and an archived link [here](https://web.archive.org/web/20170615233416/https://www.hakaimagazine.com/features/lunar-sea).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









