---
title: "Revisiting the 1998 World Cup final"
date: "2016-08-28T07:35:33"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://twitter.com/kenyanpundit/status/769800571431841792) and an archived link [here](https://web.archive.org/web/20200210033946/https://twitter.com/kenyanpundit/status/769800571431841792).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









