---
title: "It’s Time These Ancient Women Scientists Get Their Due"
date: "2020-11-22T10:35:38"
params:
  title: no # default
  date: no  # default
  expanded_url: no # default
  archived_url: no # default
output:
  html_document:
    keep_md: true
---


Find the original link [here](http://wise.nautil.us/feature/440/its-time-these-ancient-women-scientists-get-their-due?mc_cid=47b30e2be8&mc_eid=c967ee1f21) and an archived link [here](https://web.archive.org/web/20201122161938/http://wise.nautil.us/feature/440/its-time-these-ancient-women-scientists-get-their-due).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._

