---
title: "Hot Food, Fast: The Home Microwave Oven Turns 50"
date: "2017-04-02T07:47:01"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.smithsonianmag.com/innovation/hot-food-fast-home-microwave-oven-turns-50-180962545/) and an archived link [here](https://web.archive.org/web/20170317032903/http://www.smithsonianmag.com/innovation/hot-food-fast-home-microwave-oven-turns-50-180962545/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









