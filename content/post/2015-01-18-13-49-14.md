---
title: "The murder that has obsessed Italy"
date: "2015-01-18T13:49:14"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/world/2015/jan/08/-sp-the-murder-that-has-obsessed-italy?utm_content=buffer8388f&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer) and an archived link [here](https://web.archive.org/save/https://www.theguardian.com/world/2015/jan/08/-sp-the-murder-that-has-obsessed-italy?utm_content=buffer8388f&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









