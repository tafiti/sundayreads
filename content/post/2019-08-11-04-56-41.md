---
title: "Missy Elliott  the legend returns"
date: "2019-08-11T04:56:41"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.marieclaire.com/celebrity/a28250119/missy-elliott-new-album-2019/) and an archived link [here](https://web.archive.org/web/20190715174816/https://www.marieclaire.com/celebrity/a28250119/missy-elliott-new-album-2019/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









