---
title: "My Friend, Stalin’s Daughter"
date: "2015-03-08T07:53:48"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/magazine/2014/03/31/my-friend-stalins-daughter) and an archived link [here](https://web.archive.org/web/20140818210325/http://www.newyorker.com/magazine/2014/03/31/my-friend-stalins-daughter).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









