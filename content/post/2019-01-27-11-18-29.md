---
title: "Letter to a Young Queer Intellectual "
date: "2019-01-27T11:18:29"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://johannesburgreviewofbooks.com/2019/01/14/conversation-issue-letter-to-a-young-queer-intellectual-by-amatesiro-dore/) and an archived link [here](https://web.archive.org/web/20190210025959/https://johannesburgreviewofbooks.com/2019/01/14/conversation-issue-letter-to-a-young-queer-intellectual-by-amatesiro-dore/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









