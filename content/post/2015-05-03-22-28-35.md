---
title: "The People’s Pope and the Chairman of Everything"
date: "2015-05-03T22:28:35"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://lareviewofbooks.org/article/the-peoples-pope-and-the-chairman-of-everything/) and an archived link [here](https://web.archive.org/web/20190412012145/https://lareviewofbooks.org/article/the-peoples-pope-and-the-chairman-of-everything).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









