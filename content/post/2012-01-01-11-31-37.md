---
title: "Huawei: Will China conquer the world?"
date: "2012-01-01T11:31:37"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://www.theglobeandmail.com/report-on-business/rob-magazine/huawei-will-china-conquer-the-world/article2243928/) and an archived link [here](https://web.archive.org/web/20170402102825/http://www.theglobeandmail.com/report-on-business/rob-magazine/huawei-will-china-conquer-the-world/article2243928/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









