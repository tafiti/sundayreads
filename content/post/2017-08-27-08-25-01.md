---
title: "The devastation of black Wall Street"
date: "2017-08-27T08:25:01"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://daily.jstor.org/the-devastation-of-black-wall-street/?) and an archived link [here](https://web.archive.org/web/20170829074122/https://daily.jstor.org/the-devastation-of-black-wall-street/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









