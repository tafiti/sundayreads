---
title: "The Fall of the House of Clinton "
date: "2016-11-13T19:11:48"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.vanityfair.com/news/2016/11/the-fall-of-the-house-of-clinton) and an archived link [here](https://web.archive.org/web/0/https://www.vanityfair.com/news/2016/11/the-fall-of-the-house-of-clinton).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









