---
title: "Shipwrecked: A Shocking Tale of Love, Loss, and Survival in the Deep Blue Sea"
date: "2021-08-22T08:34:14"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://www.bostonmagazine.com/news/lost-at-sea/) and an archived
link
[here](http://web.archive.org/web/20210817140335/https://www.bostonmagazine.com/news/lost-at-sea/).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
