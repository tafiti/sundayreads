---
title: "What Happened To Tiger Woods? It’s The Most Vexing Question In Sports"
date: "2016-04-03T05:42:06"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.golf.com/tour-and-news/what-happened-tiger-woods-it-remains-most-vexing-question-sports?utm_source=nextdraft&utm_medium=email) and an archived link [here](https://web.archive.org/web/20160330163425/http://www.golf.com/tour-and-news/what-happened-tiger-woods-it-remains-most-vexing-question-sports).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









