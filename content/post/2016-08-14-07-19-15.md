---
title: "How Only Being Able to Use Logic to Make Decisions Destroyed a Man’s Life"
date: "2016-08-14T07:19:15"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.thecut.com/2016/06/how-only-using-logic-destroyed-a-man.html) and an archived link [here](https://web.archive.org/web/20180614233549/https://www.thecut.com/2016/06/how-only-using-logic-destroyed-a-man.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









