---
title: "The Rise and Fall of Vanilla Ice, As Told by Vanilla Ice "
date: "2020-10-11T06:49:27"
params:
  title: no # default
  postdate: no  # default
  urls_expanded_urls: no
  archivedurl: no
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theringer.com/music/2020/10/6/21494291/vanilla-ice-to-the-extreme-ice-ice-baby-history-30th-anniversary) and an archived link [here](https://web.archive.org/web/20201101032517/https://www.theringer.com/music/2020/10/6/21494291/vanilla-ice-to-the-extreme-ice-ice-baby-history-30th-anniversary).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
