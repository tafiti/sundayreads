---
title: "The Rebel Virgins and Desert Mothers Who Have Been Written Out of Christianity’s Early History"
date: "2016-01-31T07:06:08"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.atlasobscura.com/articles/the-rebel-virgins-and-desert-mothers-who-have-been-written-out-of-christianitys-early-history?utm_source=Narratively+email+list&utm_campaign=2f92dac45d-Jan_241_23_2016&utm_medium=email&utm_term=0_f944cd8d3b-2f92dac45d-66307813) and an archived link [here](https://web.archive.org/web/20200204081029/https://www.atlasobscura.com/articles/the-rebel-virgins-and-desert-mothers-who-have-been-written-out-of-christianitys-early-history?utm_source=Narratively+email+list&utm_campaign=2f92dac45d-Jan_241_23_2016&utm_medium=email&utm_term=0_f944cd8d3b-2f92dac45d-66307813).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









