---
title: "Black Body: Rereading James Baldwin’s “Stranger in the Village”"
date: "2020-05-31T10:56:19"
params:
  title: 0
  date: 0
  originalurl: 0
  archivedurl: 0
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/books/page-turner/black-body-re-reading-james-baldwins-stranger-village) and an archived link [here](https://web.archive.org/web/20200601140944/https://www.newyorker.com/books/page-turner/black-body-re-reading-james-baldwins-stranger-village).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._

