---
title: "The whitest paint is here – and it’s the coolest. Literally. - Purdue University News"
date: "2021-05-09T09:18:12"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://www.purdue.edu/newsroom/releases/2021/Q2/the-whitest-paint-is-here-and-its-the-coolest.-literally..html)
and an archived link
[here](http://web.archive.org/web/20210415181237/https://www.purdue.edu/newsroom/releases/2021/Q2/the-whitest-paint-is-here-and-its-the-coolest.-literally..html).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
