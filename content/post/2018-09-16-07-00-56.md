---
title: "Home is better than this place"
date: "2018-09-16T07:00:56"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://johannesburgreviewofbooks.com/2018/04/04/home-is-better-than-this-place-read-an-exclusive-excerpt-from-nozizwe-cynthia-jeles-forthcoming-novel-the-ones-with-purpose/) and an archived link [here](https://web.archive.org/web/20200204083455/https://johannesburgreviewofbooks.com/2018/04/04/home-is-better-than-this-place-read-an-exclusive-excerpt-from-nozizwe-cynthia-jeles-forthcoming-novel-the-ones-with-purpose/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









