---
title: "A History Of Visa — Mine Safety Disclosures"
date: "2019-09-01T08:19:58"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://minesafetydisclosures.com/blog/2019/5/29/part-l-a-history-of-visa) and an archived link [here](https://web.archive.org/web/20190603181623/http://minesafetydisclosures.com/blog/2019/5/29/part-l-a-history-of-visa).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









