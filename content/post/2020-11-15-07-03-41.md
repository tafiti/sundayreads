---
title: "The Curse of the Buried Treasure"
date: "2020-11-15T07:03:41"
params:
  title: no # default
  postdate: no  # default
  urls_expanded_urls: no
  archivedurl: no
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/magazine/2020/11/16/the-curse-of-the-buried-treasure) and an archived link [here](https://web.archive.org/web/20201114141308/https://www.newyorker.com/magazine/2020/11/16/the-curse-of-the-buried-treasure).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._