---
title: "The Secret Language of Tennis Champions"
date: "2013-12-29T09:34:32"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://nautil.us/issue/6/secret-codes/the-secret-language-of-tennis-champions) and an archived link [here](https://web.archive.org/web/20131017125414/http://nautil.us/issue/6/secret-codes/the-secret-language-of-tennis-champions).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









