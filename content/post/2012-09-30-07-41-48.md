---
title: "Dining With The Snakes"
date: "2012-09-30T07:41:48"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.discovermagazine.com/the-sciences/dining-with-the-snakes) and an archived link [here](https://web.archive.org/web/20200204074449/https://www.discovermagazine.com/the-sciences/dining-with-the-snakes).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









