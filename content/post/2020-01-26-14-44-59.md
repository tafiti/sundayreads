---
title: "Dance or die: fighting for the legacy of Bogle, the “Godfather of dancehall” "
date: "2020-01-26T14:44:59"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.1843magazine.com/1843/dance-or-die-fighting-for-the-legacy-of-bogle-the-godfather-of-dancehall) and an archived link [here](https://web.archive.org/web/20200116193614/https://www.1843magazine.com/1843/dance-or-die-fighting-for-the-legacy-of-bogle-the-godfather-of-dancehall).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









