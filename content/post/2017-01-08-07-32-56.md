---
title: "Debutante in the Amazon Jungle: A 50-Year Adventure"
date: "2017-01-08T07:32:56"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://longreads.com/2017/01/06/debutante-in-the-amazon-jungle-a-50-year-adventure/) and an archived link [here](https://web.archive.org/web/0/https://longreads.com/2017/01/06/debutante-in-the-amazon-jungle-a-50-year-adventure/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









