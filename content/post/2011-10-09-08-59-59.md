---
title: "Alexander: How Great?"
date: "2011-10-09T08:59:59"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nybooks.com/articles/2011/10/27/alexander-how-great/?pagination=false) and an archived link [here](https://web.archive.org/web/20200204073556/https://www.nybooks.com/articles/2011/10/27/alexander-how-great/?pagination=false).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









