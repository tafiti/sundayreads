---
title: "Satiate"
date: "2017-09-03T07:07:21"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://roommagazine.com/writing/satiate) and an archived link [here](https://web.archive.org/web/20170907084226/http://roommagazine.com:80/web/20170907000000*/http://roommagazine.com/writing/satiate).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









