---
title: "Remembering the exquisite exploits of Joginder Singh, Kenya’s ‘Flying Sikh’"
date: "2013-01-27T07:42:08"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nation.co.ke/sports/Kenyas-Flying-Sikh/-/1090/1663516/-/n7h8psz/-/index.html) and an archived link [here](https://web.archive.org/web/20130112045602/http://www.nation.co.ke/sports/Kenyas-Flying-Sikh/-/1090/1663516/-/n7h8psz/-/index.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









