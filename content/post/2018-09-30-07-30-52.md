---
title: "‘If the short story is a sprint, the novel is a marathon,’ and more writing advice from Lesley Nneka Arimah"
date: "2018-09-30T07:30:52"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.pbs.org/newshour/arts/if-the-short-story-is-a-sprint-the-novel-is-a-marathon-and-more-writing-advice-from-lesley-nneka-arimah) and an archived link [here](https://web.archive.org/web/20180808100237/https://www.pbs.org/newshour/arts/if-the-short-story-is-a-sprint-the-novel-is-a-marathon-and-more-writing-advice-from-lesley-nneka-arimah).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









