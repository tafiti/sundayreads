---
title: "In The Land Of Missing Persons"
date: "2016-11-20T08:06:57"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theatlantic.com/magazine/archive/2016/04/in-the-land-of-missing-persons/471477/) and an archived link [here](https://web.archive.org/web/0/https://www.theatlantic.com/magazine/archive/2016/04/in-the-land-of-missing-persons/471477/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









