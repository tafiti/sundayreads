---
title: "Behind the Scenes With Buffett’s Biographer, Alice Schroeder"
date: "2020-08-09T15:25:35"
params:
  title: 0
  date: 0
  originalurl: 0
  archivedurl: 0
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://seekingalpha.com/article/235292-behind-the-scenes-with-buffett-s-biographer-alice-schroeder) and an archived link [here](https://web.archive.org/web/20200809163359/https://seekingalpha.com/article/235292-behind-the-scenes-with-buffett-s-biographer-alice-schroeder).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
