---
title: "The Curious Case of Jesus’s Wife"
date: "2014-11-30T06:21:12"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theatlantic.com/magazine/archive/2014/12/the-curious-case-of-jesuss-wife/382227/?src=longreads&utm_campaign=buffer&utm_content=buffer6fe87&utm_medium=social&utm_source=twitter.com) and an archived link [here](https://web.archive.org/web/20200204080058/https://www.theatlantic.com/magazine/archive/2014/12/the-curious-case-of-jesuss-wife/382227/?src=longreads&utm_campaign=buffer&utm_content=buffer6fe87&utm_medium=social&utm_source=twitter.com).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









