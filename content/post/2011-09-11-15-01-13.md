---
title: "Flying While Black and Reading Aviation Books"
date: "2011-09-11T15:01:13"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://archive.boston.com/community/blogs/on_liberty/2011/08/racial_profiling_first_hand.html) and an archived link [here](https://web.archive.org/web/20160410183723/http://archive.boston.com/community/blogs/on_liberty/2011/08/racial_profiling_first_hand.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









