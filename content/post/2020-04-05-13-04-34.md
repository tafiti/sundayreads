---
title: "The secret resistance behind the world’s most dangerous cheese"
date: "2020-04-05T13:04:34"
params:
  title: 0
  date: 0
  originalurl: 0
  archivedurl: 0
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://theoutline.com/post/8843/casu-marzu-cheese-sardinia-illegal-dangerous) and an archived link [here](https://web.archive.org/web/20200404115421/https://theoutline.com/post/8843/casu-marzu-cheese-sardinia-illegal-dangerous).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._