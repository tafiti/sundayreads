---
title: "Esther Perel Is Fighting the “Tyranny of Positivity”"
date: "2021-08-22T08:42:57"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://www.gq.com/story/esther-perel-interview) and an archived
link
[here](http://web.archive.org/web/20210719121255/https://www.gq.com/story/esther-perel-interview).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
