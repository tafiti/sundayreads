---
title: "The Amazing Story of the Russian Defector Who Changed his Mind"
date: "2018-04-22T18:26:13"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.washingtonian.com/2018/02/18/the-amazing-story-of-the-russian-defector-who-changed-his-mind-vitaly-yurchenko/?) and an archived link [here](https://web.archive.org/web/20180219025557/https://www.washingtonian.com/2018/02/18/the-amazing-story-of-the-russian-defector-who-changed-his-mind-vitaly-yurchenko/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









