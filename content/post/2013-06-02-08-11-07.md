---
title: "The Extraordinary Science of Addictive Junk Food"
date: "2013-06-02T08:11:07"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nytimes.com/2013/02/24/magazine/the-extraordinary-science-of-junk-food.html?smid=tw-share&_r=0) and an archived link [here](https://web.archive.org/web/20130224034039/http://www.nytimes.com/2013/02/24/magazine/the-extraordinary-science-of-junk-food.html?smid=tw-share&_r=0).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









