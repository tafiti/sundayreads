---
title: "HE'S BEHIND YOU"
date: "2012-12-30T08:33:44"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.bbc.co.uk/blogs/adamcurtis/2012/10/hes_behind_you.html) and an archived link [here](https://web.archive.org/web/20121024184705/http://www.bbc.co.uk/blogs/adamcurtis/2012/10/hes_behind_you.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









