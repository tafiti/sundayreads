---
title: "You Are The Second Person"
date: "2014-02-09T07:52:21"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.guernicamag.com/you-are-the-second-person/) and an archived link [here](https://web.archive.org/web/20181208123553/https://www.guernicamag.com/you-are-the-second-person/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









