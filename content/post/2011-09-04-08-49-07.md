---
title: "Profiting From 9/11"
date: "2011-09-04T08:49:07"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.villagevoice.com/2011/08/31/this-week-in-the-voice-911s-biggest-winners-gourmet-rice-a-roni/) and an archived link [here](https://www.villagevoice.com/2011/08/31/this-week-in-the-voice-911s-biggest-winners-gourmet-rice-a-roni/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









