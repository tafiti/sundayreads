---
title: "Why Your Brain Hates Slowpokes"
date: "2019-01-20T09:30:51"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://nautil.us/issue/22/slow/why-your-brain-hates-slowpokes) and an archived link [here](https://web.archive.org/web/20150307003718/http://nautil.us/issue/22/slow/why-your-brain-hates-slowpokes).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









