---
title: "How to Raise a Prodigy"
date: "2018-02-11T07:29:30"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/magazine/2018/01/29/how-to-raise-a-prodigy?mbid=social_twitter) and an archived link [here](https://web.archive.org/web/20180216083449/https://www.newyorker.com/magazine/2018/01/29/how-to-raise-a-prodigy?mbid=social_twitter).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









