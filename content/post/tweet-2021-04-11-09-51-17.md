---
title: "How Apple Is Organized for Innovation"
date: "2021-04-11T09:51:17"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://hbr.org/2020/11/how-apple-is-organized-for-innovation) and an archived link [here](http://web.archive.org/web/20201022180534/https://hbr.org/2020/11/how-apple-is-organized-for-innovation).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
