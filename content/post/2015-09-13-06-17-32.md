---
title: "Murder in the Ivy League"
date: "2015-09-13T06:17:32"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://highline.huffingtonpost.com/articles/en/love-in-the-age-of-big-data/?utm_source=Narratively+email+list&utm_campaign=7bb053951b-Weekender_August_30th8_28_2015&utm_medium=email&utm_term=0_f944cd8d3b-7bb053951b-66307813) and an archived link [here](https://web.archive.org/web/20190214230945/https://narratively.com/murder-in-the-ivy-league/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









