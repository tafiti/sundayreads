---
title: "New Documents Shine Light On Euro Birth Defects"
date: "2012-05-13T08:27:43"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.spiegel.de/international/europe/euro-struggles-can-be-traced-to-origins-of-common-currency-a-831842.html) and an archived link [here](https://web.archive.org/web/20120509184543/http://www.spiegel.de/international/europe/euro-struggles-can-be-traced-to-origins-of-common-currency-a-831842.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









