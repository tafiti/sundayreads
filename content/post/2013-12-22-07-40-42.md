---
title: "Tyler Hadley’s Killer Party"
date: "2013-12-22T07:40:42"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.rollingstone.com/culture/culture-news/tyler-hadleys-killer-party-54270/) and an archived link [here](https://web.archive.org/web/20181019000815/https://www.rollingstone.com/culture/culture-news/tyler-hadleys-killer-party-54270/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









