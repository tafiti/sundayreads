---
title: "Financial Windfalls: 15 Stories of Gifts, Wins, Inheritances, and The Money That Changed Everything"
date: "2019-04-21T06:21:11"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.topic.com/financial-windfalls-15-stories-of-the-money-that-changed-everything) and an archived link [here](https://web.archive.org/web/20190220001148/https://www.topic.com/financial-windfalls-15-stories-of-the-money-that-changed-everything).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









