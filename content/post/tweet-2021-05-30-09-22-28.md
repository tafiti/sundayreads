---
title: "'If the camel is fine, our life is fine.' But Somali camel herding is in jeopardy."
date: "2021-05-30T09:22:28"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://www.nationalgeographic.com/culture/article/somali-camel-herding-in-jeopardy-if-camel-is-fine-our-life-is-fine)
and an archived link
[here](http://web.archive.org/web/20210519192111/https://www.nationalgeographic.com/culture/article/somali-camel-herding-in-jeopardy-if-camel-is-fine-our-life-is-fine).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
