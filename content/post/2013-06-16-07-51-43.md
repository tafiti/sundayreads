---
title: "How Facebook Designs the 'Perfect Empty Vessel' for Your Mind"
date: "2013-06-16T07:51:43"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theatlantic.com/technology/archive/2013/05/how-facebook-designs-the-perfect-empty-vessel-for-your-mind/275426/) and an archived link [here](https://web.archive.org/web/20130502192443/http://www.theatlantic.com/technology/archive/2013/05/how-facebook-designs-the-perfect-empty-vessel-for-your-mind/275426/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









