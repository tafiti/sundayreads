---
title: "The Collective: Managers to the (YouTube) Stars"
date: "2012-09-23T07:30:35"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.bloomberg.com/news/articles/2012-09-18/the-collective-managers-to-the-youtube-stars) and an archived link [here](https://web.archive.org/web/20160527101733/http://www.bloomberg.com/news/articles/2012-09-18/the-collective-managers-to-the-youtube-stars).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









