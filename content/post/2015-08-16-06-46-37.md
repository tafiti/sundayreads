---
title: "You’ve Run Out."
date: "2015-08-16T06:46:37"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/magazine/2008/02/11/true-crime) and an archived link [here](https://web.archive.org/web/0/https://www.newyorker.com/magazine/2008/02/11/true-crime).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









