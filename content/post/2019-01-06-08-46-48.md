---
title: "Salesforce’s Marc Benioff unplugged for two weeks, and had a revelation that could change the tech industry"
date: "2019-01-06T08:46:48"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.cnbc.com/2018/12/30/salesforce-marc-benioff-talks-tech-ethics-time-magazine-and-vacation.html) and an archived link [here](https://web.archive.org/web/20190105205403/https://www.cnbc.com/2018/12/30/salesforce-marc-benioff-talks-tech-ethics-time-magazine-and-vacation.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









