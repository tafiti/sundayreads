---
title: "Empire of the sun"
date: "2012-12-23T08:29:52"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theage.com.au/lifestyle/empire-of-the-sun-20121210-2b4an.html) and an archived link [here](https://www.theage.com.au/lifestyle/empire-of-the-sun-20121210-2b4an.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









