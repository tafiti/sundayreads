---
title: "Why the Death of Greatest Hits Albums and Reissues Is Worth Mourning"
date: "2016-05-08T07:11:43"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://pitchfork.com/features/article/9887-why-the-death-of-greatest-hits-albums-and-reissues-is-worth-mourning/) and an archived link [here](https://web.archive.org/web/20160503100356/http://pitchfork.com/features/article/9887-why-the-death-of-greatest-hits-albums-and-reissues-is-worth-mourning/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









