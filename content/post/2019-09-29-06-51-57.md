---
title: "It’s Not Always Excellent to Be Jamie Oliver"
date: "2019-09-29T06:51:57"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nytimes.com/2019/08/20/dining/jamie-oliver.html) and an archived link [here](https://web.archive.org/web/20190820091031/https://www.nytimes.com/2019/08/20/dining/jamie-oliver.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









