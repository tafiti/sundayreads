---
title: "What Does a Conductor Do?"
date: "2012-12-09T07:08:16"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://nymag.com/arts/classicaldance/classical/features/conductors-2012-1/#print) and an archived link [here](https://web.archive.org/web/20120107122935/http://nymag.com/arts/classicaldance/classical/features/conductors-2012-1#print).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









