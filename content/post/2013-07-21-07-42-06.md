---
title: "A Pickpocket’s Tale"
date: "2013-07-21T07:42:06"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/magazine/2013/01/07/a-pickpockets-tale) and an archived link [here](https://web.archive.org/web/20140722222635/http://www.newyorker.com/magazine/2013/01/07/a-pickpockets-tale).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









