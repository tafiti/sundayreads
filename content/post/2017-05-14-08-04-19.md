---
title: "Maaza Mengiste Talks about Ethiopian Women and her Forthcoming Book"
date: "2017-05-14T08:04:19"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.bookwitty.com/text/maaza-mengiste-talks-about-ethiopian-women-and-her/58ff6d4650cef70abc12c747) and an archived link [here](https://web.archive.org/web/20180626201915/https://www.bookwitty.com/text/maaza-mengiste-talks-about-ethiopian-women-and-her/58ff6d4650cef70abc12c747).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









