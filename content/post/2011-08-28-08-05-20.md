---
title: "Do You Suffer From Decision Fatigue?"
date: "2011-08-28T08:05:20"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nytimes.com/2011/08/21/magazine/do-you-suffer-from-decision-fatigue.html?_r=1) and an archived link [here](https://web.archive.org/web/20180429021640/http://www.nytimes.com/auth/login?URI=/2011/08/21/magazine/do-you-suffer-from-decision-fatigue.html&OQ=_rQ3D1&REFUSE_COOKIE_ERROR=SHOW_ERROR).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









