---
title: "Global Shortages During Coronavirus Reveal Failings of Just in Time Manufacturing   - The New York Times"
date: "2021-06-13T09:05:06"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://www.nytimes.com/2021/06/01/business/coronavirus-global-shortages.html?action=click&module=Spotlight&pgtype=Homepage)
and an archived link
[here](http://web.archive.org/web/20210601142215/https://www.nytimes.com/2021/06/01/business/coronavirus-global-shortages.html?action=click&module=Spotlight&pgtype=Homepage).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
