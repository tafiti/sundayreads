---
title: "The Talented Mr. Khater"
date: "2015-07-05T07:50:29"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.texasmonthly.com/articles/the-talented-mr-khater/) and an archived link [here](https://web.archive.org/web/20150813105213/http://www.texasmonthly.com/articles/the-talented-mr-khater/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









