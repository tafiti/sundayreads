---
title: "Condoms: global market value 2017-2023 | Statista"
date: "2021-07-18T10:50:24"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://www.statista.com/statistics/739346/condom-market-value-worldwide/)
and an archived link
[here](http://web.archive.org/web/20191210112143/https://www.statista.com/statistics/739346/condom-market-value-worldwide/).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
