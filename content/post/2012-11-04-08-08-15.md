---
title: "Clear Eyes, Full Plates, Can't Puke"
date: "2012-11-04T08:08:15"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.gq.com/story/jon-ronson-competitive-eating-contests-gq-november-2012?printable=true) and an archived link [here](https://web.archive.org/web/20150930190338/http://www.gq.com/story/jon-ronson-competitive-eating-contests-gq-november-2012?printable=true).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









