---
title: "This Little Light Of Mine – Doek!"
date: "2021-07-11T09:36:43"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://doeklitmag.com/this-little-light-of-mine/) and an
archived link
[here](http://web.archive.org/web/20200712162239/https://doeklitmag.com/this-little-light-of-mine/).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
