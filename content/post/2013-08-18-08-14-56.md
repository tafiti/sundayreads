---
title: "Inside India’s Big Fat $38 Billion Wedding Market, Part 1"
date: "2013-08-18T08:14:56"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.businessoffashion.com/articles/global-currents/inside-indias-big-fat-38-billion-wedding-market-part-1-rohit-bal-sabyasachi-mukherjee-alex-kuruvilla-vijay-singh-india-bridal-fashion-week) and an archived link [here](https://web.archive.org/web/20150403052743/http://www.businessoffashion.com/articles/global-currents/inside-indias-big-fat-38-billion-wedding-market-part-1-rohit-bal-sabyasachi-mukherjee-alex-kuruvilla-vijay-singh-india-bridal-fashion-week).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









