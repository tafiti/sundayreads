---
title: "Why Clocks Run Clockwise (And Some Watches And Clocks That Don't)"
date: "2017-11-05T08:33:20"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.hodinkee.com/articles/why-clocks-run-clockwise?utm_source=New+Daily+Newsletter+Subscribers&utm_campaign=96d841473c-EMAIL_CAMPAIGN_2017_11_03&utm_medium=email&utm_term=0_4675a5c15f-96d841473c-81810149) and an archived link [here](https://web.archive.org/web/20200204082511/https://www.hodinkee.com/articles/why-clocks-run-clockwise?utm_source=New+Daily+Newsletter+Subscribers&utm_campaign=96d841473c-EMAIL_CAMPAIGN_2017_11_03&utm_medium=email&utm_term=0_4675a5c15f-96d841473c-81810149).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









