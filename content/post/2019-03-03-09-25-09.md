---
title: "The deadly truth about a world built for men – from stab vests to car crashes"
date: "2019-03-03T09:25:09"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/lifeandstyle/2019/feb/23/truth-world-built-for-men-car-crashes) and an archived link [here](https://web.archive.org/web/20190223095237/https://www.theguardian.com/lifeandstyle/2019/feb/23/truth-world-built-for-men-car-crashes).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









