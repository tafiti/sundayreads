---
title: "Greek Finance Minister Yanis Varoufakis: ‘if I Weren’t Scared, I’d Be Awfully Dangerous’"
date: "2015-02-15T07:41:03"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/world/2015/feb/13/greece-finance-minister-yanis-varoufakis-interview-syriza-eurozone) and an archived link [here](https://web.archive.org/web/20150215011003/http://www.theguardian.com/world/2015/feb/13/greece-finance-minister-yanis-varoufakis-interview-syriza-eurozone).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









