---
title: "Tina Turner is having the time of her life"
date: "2019-09-22T06:55:56"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nytimes.com/2019/09/09/theater/tina-turner-musical.html) and an archived link [here](https://web.archive.org/web/20190909091045/https://www.nytimes.com/2019/09/09/theater/tina-turner-musical.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









