---
title: "The lost story of Nearest Green, the slave who taught Jack Daniel how to make whiskey - CBS News
    "
date: "2020-01-12T14:25:08"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.cbsnews.com/news/nearest-green-slave-who-taught-jack-daniel-how-to-make-whiskey/) and an archived link [here](https://web.archive.org/web/20171128220203/https://www.cbsnews.com/news/nearest-green-slave-who-taught-jack-daniel-how-to-make-whiskey/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









