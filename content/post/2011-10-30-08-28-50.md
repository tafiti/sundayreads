---
title: "The Man Who Isn't There"
date: "2011-10-30T08:28:50"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://sportsillustrated.cnn.com/vault/article/magazine/MAG1191566/1/index.htm) and an archived link [here](https://web.archive.org/web/20111027200741/http://sportsillustrated.cnn.com/vault/article/magazine/MAG1191566/1/index.htm).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









