---
title: "The Beauty of the Airline Baggage Tag"
date: "2012-10-07T07:34:17"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://www.slate.com/articles/life/design/2012/10/airline_baggage_tags_how_their_brilliant_design_gets_bags_from_point_a_to_point_b_.single.html) and an archived link [here](https://web.archive.org/web/20121004173729/http://www.slate.com/articles/life/design/2012/10/airline_baggage_tags_how_their_brilliant_design_gets_bags_from_point_a_to_point_b_.single.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









