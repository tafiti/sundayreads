---
title: "The Child Preachers of Brazil"
date: "2015-06-21T20:29:05"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nytimes.com/2015/06/14/magazine/the-child-preachers-of-brazil.html?_r=2) and an archived link [here](https://web.archive.org/web/20150616072131/http://www.nytimes.com/2015/06/14/magazine/the-child-preachers-of-brazil.html?_r=3).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









