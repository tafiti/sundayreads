---
title: "The Anchor"
date: "2014-07-27T07:38:21"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/magazine/2014/04/21/the-anchor) and an archived link [here](https://web.archive.org/web/20140725062750/http://www.newyorker.com/magazine/2014/04/21/the-anchor).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









