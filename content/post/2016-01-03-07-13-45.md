---
title: "The Kinshasa Cowboys: How Buffalo Bill Started A Subculture In Congo"
date: "2016-01-03T07:13:45"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://edition.cnn.com/2015/12/08/africa/kinshasa-cowboys-bills/index.html) and an archived link [here](https://web.archive.org/web/0/https://edition.cnn.com/2015/12/08/africa/kinshasa-cowboys-bills/index.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









