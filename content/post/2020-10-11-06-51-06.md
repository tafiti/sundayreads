---
title: "The Deadly Story of Poison "
date: "2020-10-11T06:51:06"
params:
  title: no # default
  postdate: no  # default
  urls_expanded_urls: no
  archivedurl: no
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://www.popularmechanics.com/science/health/a34100092/history-of-poison/) and an archived link [here](https://web.archive.org/web/20201106095900/https://www.popularmechanics.com/science/health/a34100092/history-of-poison/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
