---
title: "Social media and the great recipe explosion: does more mean better?"
date: "2017-07-02T07:49:03"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/lifeandstyle/2017/jun/18/great-recipe-explosion-social-media-does-more-mean-better-instagram-pinterest) and an archived link [here](https://web.archive.org/save/https://www.theguardian.com/lifeandstyle/2017/jun/18/great-recipe-explosion-social-media-does-more-mean-better-instagram-pinterest).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









