---
title: "Why We Sleep Together"
date: "2015-03-08T07:46:47"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theatlantic.com/health/archive/2014/06/why-we-sleep-together/371477/) and an archived link [here](https://web.archive.org/save/https://www.businessinsider.com/why-people-sleep-together-2015-3).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









