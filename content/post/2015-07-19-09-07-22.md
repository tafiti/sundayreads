---
title: "The story of Lagos’ ill-fated 1976 Professional Tennis Tournament"
date: "2015-07-19T09:07:22"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://africasacountry.com/2015/07/the-story-of-lagos-ill-fated-1976-professional-tennis-tournament/) and an archived link [here](https://web.archive.org/web/20150724153712/http://africasacountry.com/2015/07/the-story-of-lagos-ill-fated-1976-professional-tennis-tournament/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









