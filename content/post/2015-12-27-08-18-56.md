---
title: "China: The Superpower of Mr. Xi"
date: "2015-12-27T08:18:56"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://www.chinafile.com/library/nyrb-china-archive/china-superpower-mr-xi) and an archived link [here](https://web.archive.org/save/http://www.chinafile.com/library/nyrb-china-archive/china-superpower-mr-xi).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









