---
title: "What would Jesus disrupt"
date: "2017-04-09T19:00:52"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.bloomberg.com/news/features/2017-04-05/what-would-jesus-disrupt) and an archived link [here](https://web.archive.org/web/20170405141726/https://www.bloomberg.com/news/features/2017-04-05/what-would-jesus-disrupt).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









