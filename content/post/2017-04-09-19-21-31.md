---
title: "Where Prince Charles Went Wrong"
date: "2017-04-09T19:21:31"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/magazine/2017/04/10/where-prince-charles-went-wrong?mbid=social_twitter) and an archived link [here](https://web.archive.org/web/20170411215142/http://www.newyorker.com/magazine/2017/04/10/where-prince-charles-went-wrong?mbid=social_twitter).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









