---
title: "Queens of Infamy: Josephine Bonaparte, from Malmaison to More-Than-Monarch"
date: "2019-04-14T13:41:18"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://longreads.com/2019/04/04/queens-of-infamy-josephine-bonaparte-part-two/) and an archived link [here](https://web.archive.org/web/20190404150413/https://longreads.com/2019/04/04/queens-of-infamy-josephine-bonaparte-part-two/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









