---
title: "Coke Engineers Its Orange Juice—With an Algorithm"
date: "2013-07-07T07:35:47"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.bloomberg.com/news/articles/2013-01-31/coke-engineers-its-orange-juice-with-an-algorithm#p1) and an archived link [here](https://web.archive.org/web/20160329060712/http://www.bloomberg.com/news/articles/2013-01-31/coke-engineers-its-orange-juice-with-an-algorithm#p1).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









