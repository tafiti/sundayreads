---
title: "Are Psychopaths Attracted to Other Psychopaths?"
date: "2019-02-03T08:53:45"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://blogs.scientificamerican.com/beautiful-minds/who-finds-psychopaths-hot/) and an archived link [here](https://web.archive.org/web/20190113225258/https://blogs.scientificamerican.com/beautiful-minds/who-finds-psychopaths-hot/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









