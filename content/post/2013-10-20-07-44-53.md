---
title: "The Art Of Not Belonging – Guernica"
date: "2013-10-20T07:44:53"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.guernicamag.com/the-art-of-not-belonging/) and an archived link [here](https://web.archive.org/web/20170506203434/https://www.guernicamag.com/the-art-of-not-belonging/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









