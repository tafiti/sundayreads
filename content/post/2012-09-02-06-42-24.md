---
title: "By the script"
date: "2012-09-02T06:42:24"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theage.com.au/lifestyle/by-the-script-20120813-243iq.html) and an archived link [here](https://web.archive.org/web/20200206110204/https:/www.theage.com.au/lifestyle/by-the-script-20120813-243iq.html?js-chunk-not-found-refresh=true).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









