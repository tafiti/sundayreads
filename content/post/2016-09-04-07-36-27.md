---
title: "Moving To The Beat: The Rise And Fall of The Funkees"
date: "2016-09-04T07:36:27"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://daily.bandcamp.com/features/the-funkees-interview) and an archived link [here](https://web.archive.org/web/20200204081548/https://daily.bandcamp.com/features/the-funkees-interview).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









