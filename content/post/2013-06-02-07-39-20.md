---
title: "THE LAST WORDS OF FELA ANIKULAPO KUTI"
date: "2013-06-02T07:39:20"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://chimurengachronic.co.za/when-you-kill-us-we-rule/) and an archived link [here](https://web.archive.org/web/20130325102652/http://www.chimurengachronic.co.za/when-you-kill-us-we-rule/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









