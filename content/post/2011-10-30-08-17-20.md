---
title: "Cargill - Inside the giant that rules the food business"
date: "2011-10-30T08:17:20"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://money.cnn.com/2011/10/24/news/companies/cargill_food_business.fortune/index.htm) and an archived link [here](https://web.archive.org/web/20111029003904/http://money.cnn.com/2011/10/24/news/companies/cargill_food_business.fortune/index.htm).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









