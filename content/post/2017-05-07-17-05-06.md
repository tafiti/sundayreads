---
title: "Chris Rock in a Hard Place: On Infidelity, His New Tour and Starting Over"
date: "2017-05-07T17:05:06"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.rollingstone.com/tv/tv-features/chris-rock-in-a-hard-place-on-infidelity-his-new-tour-and-starting-over-114653/) and an archived link [here](https://web.archive.org/web/20180812053628/https://www.rollingstone.com/tv/tv-features/chris-rock-in-a-hard-place-on-infidelity-his-new-tour-and-starting-over-114653/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









