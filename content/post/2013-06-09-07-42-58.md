---
title: "Whoomp! (There It Was)"
date: "2013-06-09T07:42:58"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.5280.com/2013/05/whoomp-there-it-was/) and an archived link [here](https://web.archive.org/web/20180517060826/http://www.5280.com/2013/05/whoomp-there-it-was/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









