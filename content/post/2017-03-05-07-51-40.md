---
title: "Women Can’t Be Heroes"
date: "2017-03-05T07:51:40"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://www.republic.com.ng/vol1-no1/the-erasure-of-female-pain/) and an archived link [here](https://web.archive.org/web/0/http://www.republic.com.ng/vol1-no1/the-erasure-of-female-pain/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









