---
title: "Adrian Carton de Wiart: The unkillable soldier"
date: "2015-01-11T08:18:07"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.bbc.com/news/magazine-30685433?utm_source=nextdraft&utm_medium=email) and an archived link [here](https://web.archive.org/web/20150109045316/http://www.bbc.com/news/magazine-30685433?utm_source=nextdraft&utm_medium=email).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









