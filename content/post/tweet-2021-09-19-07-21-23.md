---
title: "Rain Boots, Turning Tides, and the Search for a Missing Boy | WIRED"
date: "2021-09-19T07:21:23"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://www.wired.com/story/search-missing-boy-dylan-ehler-nova-scotia/)
and an archived link [here](NA).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
