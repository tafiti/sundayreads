---
title: "The Body Language Of Power"
date: "2017-08-13T08:50:20"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.handelsblatt.com/today/politics/angela-merkel-the-body-language-of-power/23571566.html?utm_term=0_4675a5c15f-6cabd0937a-81810149&utm_campaign=6cabd0937a-EMAIL_CAMPAIGN_2017_08_11&utm_source=New%20Daily%20Newsletter%20Subscribers&utm_medium=email&ticket=ST-2549113-K5eVuCrRdwUby9cKW9Rc-ap3) and an archived link [here](https://web.archive.org/web/20200206200143/https://www.handelsblatt.com/today/politics/angela-merkel-the-body-language-of-power/23571566.html?utm_term=0_4675a5c15f-6cabd0937a-81810149&utm_campaign=6cabd0937a-EMAIL_CAMPAIGN_2017_08_11&utm_source=New%2520Daily%2520Newsletter%2520Subscribers&utm_medium=email&ticket=ST-3566671-jbZHnOoH4wRqVJGcTun1-ap5).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









