---
title: "I come from a place on your bucket list"
date: "2017-05-07T17:06:40"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://granta.com/bucket-list-kapoor/) and an archived link [here](https://web.archive.org/web/0/https://granta.com/bucket-list-kapoor/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









