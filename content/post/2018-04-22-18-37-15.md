---
title: "What do I mean by Skin in the Game? My Own Version"
date: "2018-04-22T18:37:15"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://medium.com/incerto/what-do-i-mean-by-skin-in-the-game-my-own-version-cc858dc73260) and an archived link [here](https://web.archive.org/save/https://docs.google.com/spreadsheets/d/1zY23n32U8gaHRYszQtoFJVYAf5m_dVTb_CTwLr_sOQA/edit?ts=5e3b16d8#gid=1091518929).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









