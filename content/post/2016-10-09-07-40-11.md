---
title: "The Medical Mystery Of Hair That Whitens Overnight"
date: "2016-10-09T07:40:11"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theatlantic.com/health/archive/2016/09/canities-subita/500576/?utm_medium=email&utm_source=nextdraft) and an archived link [here](https://web.archive.org/web/20160920181033/http://www.theatlantic.com/health/archive/2016/09/canities-subita/500576/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









