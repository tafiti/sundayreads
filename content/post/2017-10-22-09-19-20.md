---
title: "How flying seriously messes with your mind"
date: "2017-10-22T09:19:20"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.bbc.com/future/article/20170919-how-flying-seriously-messes-with-your-mind) and an archived link [here](https://web.archive.org/web/20191031220326/https://www.bbc.com/future/article/20170919-how-flying-seriously-messes-with-your-mind).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









