---
title: "Skipping the 13th Floor"
date: "2020-11-22T10:48:52"
params:
  title: no # default
  date: no  # default
  expanded_url: no # default
  archived_url: no # default
output:
  html_document:
    keep_md: true
---


Find the original link [here](https://www.theatlantic.com/technology/archive/2015/02/skipping-the-13th-floor/385448/) and an archived link [here](https://web.archive.org/web/20201113211007/https://www.theatlantic.com/technology/archive/2015/02/skipping-the-13th-floor/385448/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._

