---
title: "The Rules of the Game"
date: "2013-02-17T07:58:08"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.vqronline.org/articles/rules-game) and an archived link [here](https://web.archive.org/web/20140217043452/http://www.vqronline.org/articles/rules-game).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









