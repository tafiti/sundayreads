---
title: "Libya: In Search of a Strongman"
date: "2013-09-29T07:32:42"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nybooks.com/daily/2013/09/26/libya-next-colonel/) and an archived link [here](https://web.archive.org/web/20161214182316/http://www.nybooks.com/daily/2013/09/26/libya-next-colonel/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









