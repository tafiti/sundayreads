---
title: "Winning Isn’t Everything"
date: "2015-11-22T07:27:07"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.politico.com/magazine/story/2015/10/john-weaver-consultant-213273) and an archived link [here](https://web.archive.org/save/https://www.politico.com/magazine/story/2015/10/john-weaver-consultant-213273).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









