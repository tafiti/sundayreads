---
title: "There’s an Entire Industry Dedicated to Making Foods Crispy, and It Is WILD"
date: "2020-03-08T10:32:45"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.bonappetit.com/story/crispy) and an archived link [here](https://web.archive.org/web/20200308153144/https://www.bonappetit.com/story/crispy).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._