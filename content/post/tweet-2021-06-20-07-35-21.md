---
title: "Smokin’ Cy DeVry Was the Original Tiger King"
date: "2021-06-20T07:35:21"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://narratively.com/smokin-cy-devry-was-the-original-tiger-king/)
and an archived link
[here](http://web.archive.org/web/20201124231333/https://narratively.com/smokin-cy-devry-was-the-original-tiger-king/).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
