---
title: "Puff Daddy’s ‘No Way Out’ Turns 20"
date: "2017-12-24T08:41:58"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.passionweiss.com/2017/06/30/uncontrollable-flames-puff-daddys-no-way-out-turns-20/) and an archived link [here](https://web.archive.org/web/0/https://www.passionweiss.com/2017/06/30/uncontrollable-flames-puff-daddys-no-way-out-turns-20/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









