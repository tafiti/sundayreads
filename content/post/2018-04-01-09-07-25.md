---
title: "Pretending To Be Okay"
date: "2018-04-01T09:07:25"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](http://www.espn.com/espnw/feature/22832792/uconn-coach-geno-auriemma-only-pretending-okay) and an archived link [here](https://web.archive.org/web/20180323160046/http://www.espn.com/espnw/feature/22832792/uconn-coach-geno-auriemma-only-pretending-okay).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









