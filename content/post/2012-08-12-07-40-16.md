---
title: "Usain's Bolthole: Life In The Slow Lane In Trelawny, Jamaica"
date: "2012-08-12T07:40:16"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/travel/2012/aug/03/jamaica-falmouth-trelawny-usain-bolt) and an archived link [here](https://web.archive.org/web/20150921231721/http://www.theguardian.com/travel/2012/aug/03/jamaica-falmouth-trelawny-usain-bolt).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









