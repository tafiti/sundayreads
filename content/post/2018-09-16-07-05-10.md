---
title: "The Ugly History Of Beautiful Things: Perfume"
date: "2018-09-16T07:05:10"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://longreads.com/2018/09/10/ugly-history-of-beautiful-things-perfume/) and an archived link [here](https://web.archive.org/web/20180910134019/https://longreads.com/2018/09/10/ugly-history-of-beautiful-things-perfume/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









