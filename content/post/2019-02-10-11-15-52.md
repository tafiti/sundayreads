---
title: "A Suspense Novelist's Trail of Deceptions "
date: "2019-02-10T11:15:52"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.newyorker.com/magazine/2019/02/11/a-suspense-novelists-trail-of-deceptions) and an archived link [here](https://web.archive.org/web/20190204114247/https://www.newyorker.com/magazine/2019/02/11/a-suspense-novelists-trail-of-deceptions).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









