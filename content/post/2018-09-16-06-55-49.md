---
title: "‘The Personality Brokers’ Conjures the Mother and Daughter Who Helped Us Think of Ourselves as Types"
date: "2018-09-16T06:55:49"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nytimes.com/2018/08/29/books/review-personality-brokers-myers-briggs-merve-emre.html?smid=tw-nytimes&smtyp=cur) and an archived link [here](https://web.archive.org/save/https://www.nytimes.com/2018/08/29/books/review-personality-brokers-myers-briggs-merve-emre.html?smid=tw-nytimes&smtyp=cur).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









