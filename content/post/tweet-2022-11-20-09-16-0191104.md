---
title: "It’s time to get lost - Ozan Varol"
date: "2022-11-20T09:16:01"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output: 
  md_document:
    preserve_yaml: true
---

Find the original link
[here](https://ozanvarol.com/its-time-to-get-lost/?utm_source=substack&utm_medium=email)
and an archived link
[here](http://web.archive.org/web/20221120091718/https://ozanvarol.com/its-time-to-get-lost/?utm_source=substack&utm_medium=email).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
