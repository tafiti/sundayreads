---
title: "Ode to Gray"
date: "2018-08-26T07:36:47"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theparisreview.org/blog/2018/08/21/ode-to-gray/) and an archived link [here](https://web.archive.org/web/20180821180323/https://www.theparisreview.org/blog/2018/08/21/ode-to-gray/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









