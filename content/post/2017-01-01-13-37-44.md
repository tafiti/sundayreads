---
title: "Why time management is ruining our lives"
date: "2017-01-01T13:37:44"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/technology/2016/dec/22/why-time-management-is-ruining-our-lives) and an archived link [here](https://web.archive.org/web/20161222072025/https://www.theguardian.com/technology/2016/dec/22/why-time-management-is-ruining-our-lives).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









