---
title: "Tumbling on success: How Tumblr's David Karp built a £500 million empire"
date: "2012-02-12T09:00:05"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.wired.co.uk/article/tumbling-on-success) and an archived link [here](https://web.archive.org/web/0/https://www.wired.co.uk/article/tumbling-on-success).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









