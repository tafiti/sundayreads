---
title: "The audacity of Hope Solo"
date: "2019-06-30T13:44:24"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.elle.com/culture/a27891036/hope-solo-soccer-fifa-womens-world-cup/) and an archived link [here](https://web.archive.org/web/20190612064141/https://www.elle.com/culture/a27891036/hope-solo-soccer-fifa-womens-world-cup/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









