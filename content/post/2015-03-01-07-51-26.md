---
title: "When Did Obama Give Up?"
date: "2015-03-01T07:51:26"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://foreignpolicy.com/2015/02/26/when-did-obama-give-up-speeches/) and an archived link [here](https://web.archive.org/save/https://foreignpolicy.com/2015/02/26/when-did-obama-give-up-speeches/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









