---
title: "Patti LaBelle, the Doyenne of Philadelphia Soul "
date: "2020-12-06T07:03:32"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://www.nytimes.com/2020/11/30/t-magazine/patti-labelle-philadelphia-soul.html) and an archived link [here](https://web.archive.org/web/20201130102010/https://www.nytimes.com/2020/11/30/t-magazine/patti-labelle-philadelphia-soul.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
