---
title: "Teju Cole: 'two Drafts Of A Tweet? Insufferable. But When I Tweet I'm Still A Writer'"
date: "2014-07-13T08:09:17"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/culture/2014/jun/21/teju-cole-every-day-thief-interview) and an archived link [here](https://web.archive.org/web/20140621164924/http://www.theguardian.com/culture/2014/jun/21/teju-cole-every-day-thief-interview).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









