---
title: "The case for opsimaths. Maybe late bloomers aren't so late - The Common Reader"
date: "2021-02-21T09:23:08"
params:
  title: no # default
  postdate: no  # default
  url_expanded_url: no
  archived_url: no
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://commonreader.substack.com/p/the-case-for-opsimaths-maybe-late) and an archived link [here](http://web.archive.org/web/20210216001023/https://commonreader.substack.com/p/the-case-for-opsimaths-maybe-late).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
