---
title: "Putin: A Russian Spy Story review – 'schoolyard thug' who became an unstoppable leader"
date: "2020-07-26T15:26:39"
params:
  title: 0
  date: 0
  originalurl: 0
  archivedurl: 0
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theguardian.com/tv-and-radio/2020/mar/23/putin-a-russian-spy-story-review-schoolyard-thug-who-became-an-unstoppable-leader) and an archived link [here](https://web.archive.org/web/20200729170048/https://www.theguardian.com/tv-and-radio/2020/mar/23/putin-a-russian-spy-story-review-schoolyard-thug-who-became-an-unstoppable-leader).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
