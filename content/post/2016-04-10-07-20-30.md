---
title: "Can an Outsider Ever Truly Become Amish?"
date: "2016-04-10T07:20:30"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.atlasobscura.com/articles/can-an-outsider-ever-truly-become-amish) and an archived link [here](https://web.archive.org/web/0/https://www.atlasobscura.com/articles/can-an-outsider-ever-truly-become-amish).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









