---
title: "My Inheritance Was My Father’s Last Lesson To Me, And I Am Still Learning It"
date: "2018-02-04T08:22:03"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.buzzfeednews.com/article/alexanderchee/my-inheritance-was-my-fathers-last-lesson-to-me-and-i-am#.ytKKRqbAY2) and an archived link [here](https://web.archive.org/web/20180828205838/https://www.buzzfeednews.com/article/alexanderchee/my-inheritance-was-my-fathers-last-lesson-to-me-and-i-am#.ytKKRqbAY2).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









