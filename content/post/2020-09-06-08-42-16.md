---
title: "On Sacred Ground"
date: "2020-09-06T08:42:16"
params:
  title: no # default
  postdate: no  # default
  urls_expanded_urls: no
  archivedurl: no
output:
  html_document:
    keep_md: true
---




Find the original link [here](https://www.oxfordamerican.org/magazine/item/1919-on-sacred-ground) and an archived link [here](https://web.archive.org/web/20200805045608/https://www.oxfordamerican.org/magazine/item/1919-on-sacred-ground).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._
