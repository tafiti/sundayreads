---
title: "How Russia's Eternal President Has Changed His Country"
date: "2018-03-18T07:46:15"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.spiegel.de/international/europe/the-vladimir-putin-system-russia-and-its-eternal-president-a-1197076.html) and an archived link [here](https://web.archive.org/web/20180311184028/http://www.spiegel.de/international/europe/the-vladimir-putin-system-russia-and-its-eternal-president-a-1197076.html).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









