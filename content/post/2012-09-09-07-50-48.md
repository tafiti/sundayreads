---
title: "Searching For the Godmother of Crime"
date: "2012-09-09T07:50:48"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.maxim.com/maxim-man/searching-godmother-crime) and an archived link [here](https://web.archive.org/web/20160120000358/http://www.maxim.com/maxim-man/searching-godmother-crime).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









