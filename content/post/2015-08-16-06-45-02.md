---
title: "What the nose knows"
date: "2015-08-16T06:45:02"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://mosaicscience.com/story/what-nose-knows/) and an archived link [here](https://web.archive.org/web/20150806003312/http://mosaicscience.com/story/what-nose-knows).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









