---
title: "A Polygamist Cult’s Last Stand: The Rise and Fall of Warren Jeffs"
date: "2016-08-14T07:30:59"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.rollingstone.com/politics/politics-news/a-polygamist-cults-last-stand-the-rise-and-fall-of-warren-jeffs-230800/) and an archived link [here](https://web.archive.org/save/https://www.rollingstone.com/politics/politics-news/a-polygamist-cults-last-stand-the-rise-and-fall-of-warren-jeffs-230800/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









