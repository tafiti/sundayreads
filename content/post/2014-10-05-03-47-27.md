---
title: "How Ebola sped out of control"
date: "2014-10-05T03:47:27"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.washingtonpost.com/sf/national/2014/10/04/how-ebola-sped-out-of-control/?utm_term=.ba6d76df832d) and an archived link [here](https://web.archive.org/save/https://www.washingtonpost.com/sf/national/2014/10/04/how-ebola-sped-out-of-control/?utm_term=.ba6d76df832d).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









