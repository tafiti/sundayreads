---
title: "Upwardly Mobile"
date: "2013-01-20T10:32:19"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://caravanmagazine.in/reportage/upwardly-mobile?page=0,0) and an archived link [here](https://web.archive.org/web/20130129043029/http://caravanmagazine.in/reportage/upwardly-mobile?page=0,0).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









