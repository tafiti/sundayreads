---
title: "The Big Business of Big Hits: How Blockbusters Conquered Movies, TV, and Music"
date: "2013-12-29T09:26:34"
params:
  title: 0 
  date: 0 	  
  originalurl: 0
  archivedurl: 0
output: 
  html_document:
    keep_md: true
---




Find the original link [here](https://www.theatlantic.com/business/archive/2013/10/the-big-business-of-big-hits-how-blockbusters-conquered-movies-tv-and-music/280298/) and an archived link [here](https://web.archive.org/web/20131014041948/http://www.theatlantic.com/business/archive/2013/10/the-big-business-of-big-hits-how-blockbusters-conquered-movies-tv-and-music/280298/).

_Note: If the link above does not work, please search the web using the title provided above. To access the raw tweets, check Ory Okolloh's [twitter account](https://twitter.com/kenyanpundit) or [this](https://gitlab.com/tafiti/sundayreads) repository on GitLab._









