---
date: "2021-12-19T09:29:37"
output:
  md_document:
    preserve_yaml: true
params:
  archived_url: no
  postdate: no
  title: no
  url_expanded_url: no
title: "‘Buy the Constitution’ Aftermath: Everyone Very Mad, Confused,
  Losing Lots of Money, Fighting, Crying, Etc."
---

Find the original link
[here](https://www.vice.com/en/article/qjb8av/constitutiondao-aftermath-everyone-very-mad-confused-losing-lots-of-money-fighting-crying-etc)
and an archived link
[here](http://web.archive.org/web/20211122212258/https://www.vice.com/en/article/qjb8av/constitutiondao-aftermath-everyone-very-mad-confused-losing-lots-of-money-fighting-crying-etc).

*Note: If the link above does not work, please search the web using the
title provided above. To access the raw tweets, check Ory Okolloh’s
[twitter account](https://twitter.com/kenyanpundit) or
[this](https://gitlab.com/tafiti/sundayreads) repository on GitLab.*
