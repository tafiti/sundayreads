---
title: "Sunday Reads"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Load necessary libraries. 

```{r, include=FALSE}
library(rtweet)
library(rvest)
library(wayback)
library(tidyverse)
library(mgsub)
library(xml2)
```


This code chunck fetches tweets and filters those marked as "Read #1 - #5" a few hours after Sunday midday UTC using the `rtweet` package. It then creates new variables (using `tidyverse's` mutate function), fetches titles from respective urls (using `rvest` package functions), checks internet archive for existing saved urls (using `wayback` package functions), if existing it fetches that as an archived url and stitches everything together as a markdown document. This `.md` document is passed on to `hugo`. Hugo generates a static site which is pushed to Gitlab and using ci/cd it is published online on `sundayreads.org` 

To do:

[ ] ci/cd. Add a cronjob for sunday UTC (to keep lubridate 'today() function working). Every 6pm Nairobi time (1500UTC) (11am Toronto).

[ ] if no archive, create one and fetch the new archived url. 


[ ] Add first paragraph of the article: https://stackoverflow.com/questions/59580096/webscraping-with-r-and-rvest

[ ] Paginate list of articles

[ ] Add search functions

[ ] Add a raw tweets page

[ ] Fetch tweets once and save them locally instead instead of calling the function multiple times. 


```{r}
sundayreads = get_timeline("kenyanpundit", 
                           n = 100000, 
                           include_rts = FALSE, 
                           parse = TRUE)
```



```{r, include=FALSE}
sundayreads_blog <- sundayreads %>% 
  dplyr::filter(str_detect(text, "Read #")) %>%  
  mutate(
    postdate = format(as.POSIXct(created_at, "UTC"), "%FT%T"), .after = text,
    mddate = mgsub(postdate, c(":", "T"), c("-", "-")),
    trimmed_sid = as.numeric(substr(status_id, nchar(status_id) - 4, nchar(status_id))),
    filename = str_c("tweet-", mddate, trimmed_sid, ".md"),
    title = sapply(urls_expanded_url, function(url){
  tryCatch(
    url %>%
      as.character() %>% 
      read_html() %>% 
      html_node("title") %>% 
      html_text(), 
    error = function(e){NA}    # a function that returns NA regardless of what it's passed
  )
}),
  archived_url = sapply(urls_expanded_url, function(meme){
  tryCatch(
    meme %>%
      as.character() %>%
      get_mementos() %>%
      filter(rel == "first memento") %>%
      select(link),
    error = function(e){NA}    # a function that returns NA regardless of what it's passed
  )
})) 
```

The resultant df might have missing or inconcsistent observations. We need to clean it before runnign the md_generator function. 

```{r}
sundayreads_blog$title
```


```{r}
sundayreads_blog[15, "text"] = 'Read #1. Aliou Cissé on African Soccer, World Cup Places and Lost Generations (let’s go Teranga!): https://t.co/3mTuCJkjUr'
sundayreads_blog[1, "title"] = 'The first wound for all of us who are classified as “black” is empire'
sundayreads_blog[3, "title"] = 'How Did 🍆 Become Our Default Sex Symbol?'
sundayreads_blog[5, "title"] = 'Black is beautiful'
sundayreads_blog[9, "title"] = 'Ginga - the essence of Brazilian football through the years'
sundayreads_blog[14, "title"] = 'The brain undergoes a great rewiring after age 40 - Big Think'
sundayreads_blog[15, "title"] = 'Aliou Cissé on African Soccer, World Cup Places and Lost Generations'
sundayreads_blog[20, "title"] = 'The Tale of a Crypto Executive Who Wasn’t Who He Said He Was'
```



I separated the code chunks to review the mutations before generating blogdown content. 

Case in point. What if the string we use to detect the tweets of interest ("Read #") is missing in some of the posts? That may mess with our observations. 
To ensure we capture all the relevant posts, we ensure the `string` pattern covers all the posts. 

`dplyr::filter(created_at > Sys.Date() & str_detect(text, "Read #|Game")) %>%`
compared to 
`dplyr::filter(created_at > Sys.Date() & str_detect(text, "Read #")) %>%`

You can read more on strings here: 
- Package notes: https://r4ds.had.co.nz/strings.html
- SO example: https://stackoverflow.com/a/50163558

```{r, include=FALSE}
sundayreads_blog %>% 
  select("filename", "created_at", "title", "text", "urls_expanded_url", "mddate", "postdate", "archived_url", "status_id", "favorite_count", "retweet_count", "reply_count") %>% 
  pwalk(~ rmarkdown::render("template.Rmd", output_file = ..1, output_dir = "../content/post",
                            params = list(title = ..3, postdate = ..7, url_expanded_url = ..5, archived_url = ..8)))
```


Generate a preview site: This is a necessary step in reviewing any changes you have made on the project. 

```{r}
blogdown::serve_site()
```

If you want to contribute, open an issue if the proposed changes are significant for a brief discussion. Send a Pull Request if the issue will not change the project significantly. 

```{r}
blogdown::stop_server()
```


```{r}
# Build the site from the console, else you may get `Error: module "hugo-lithium" not found;`. 

blogdown::build_site()
```






